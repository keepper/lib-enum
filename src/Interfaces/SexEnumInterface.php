<?php
namespace Keepper\Lib\Enum\Interfaces;

interface SexEnumInterface extends EnumInterface {

	/**
	 * Мужской пол
	 */
	const MALE = 'male';

	/**
	 * Женский пол
	 */
	const FEMALE = 'female';

	/**
	 * Возвращает истину, в случае, если внутреннее значение
	 * выставленов "Мужской пол" @see SexEnumInterface::MALE
	 * @return bool
	 */
	public function isMale(): bool;

	/**
	 * Возвращает истину, в случае, если внутреннее значение
	 * выставленов "Женский пол" @see SexEnumInterface::FEMALE
	 * @return bool
	 */
	public function isFemale(): bool;
}