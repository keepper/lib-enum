<?php
namespace Keepper\Lib\Enum\Interfaces;

interface EnumInterface {
	/**
	 * Возвращает значение
	 * @return string
	 */
	public function __toString(): string;

	/**
	 * Возвращает истину если значение соответствует указанному или указанным
	 * @param string $type
	 * @return bool
	 */
	public function is(string ...$type): bool;

	/**
	 * Возвращает истину если значение не соответствует указанному или указанным
	 * @param string $type
	 * @return bool
	 */
	public function isNot(string ...$type): bool;
}