<?php
namespace Keepper\Lib\Enum;

use Keepper\Lib\Enum\Interfaces\EnumInterface;

class Enum implements EnumInterface {
	protected $value = '';

	public function __construct(string $value) {
		$this->value = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function __toString(): string {
		return $this->value;
	}

	/**
	 * @inheritdoc
	 */
	public function is(string ...$type): bool {
		return in_array($this->value, $type);
	}

	/**
	 * @inheritdoc
	 */
	public function isNot(string ...$type): bool {
		return !in_array($this->value, $type);
	}
}