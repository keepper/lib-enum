<?php
namespace Keepper\Lib\Enum;

use Keepper\Lib\Enum\Interfaces\SexEnumInterface;

class SexEnum extends Enum implements SexEnumInterface {

	/**
	 * @inheritdoc
	 */
	public function isMale(): bool {
		return $this->is(self::MALE);
	}

	/**
	 * @inheritdoc
	 */
	public function isFemale(): bool {
		return $this->is(self::FEMALE);
	}

	/**
	 * Возвращает мужской пол
	 * @return SexEnumInterface
	 */
	static public function male(): SexEnumInterface {
		return new self(self::MALE);
	}

	/**
	 * Возвращает женский пол
	 * @return SexEnumInterface
	 */
	static public function female(): SexEnumInterface {
		return new self(self::FEMALE);
	}
}