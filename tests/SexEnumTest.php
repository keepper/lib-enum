<?php
namespace Keepper\Lib\Enum\Tests;

use Keepper\Lib\Enum\Interfaces\SexEnumInterface;
use Keepper\Lib\Enum\SexEnum;

class SexEntityTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataPropviderForIsMale
	 */
	public function testIsMale(string $value, string $expectedResult) {
		$sex = new SexEnum($value);
		$this->assertEquals($expectedResult, $sex->isMale());
	}

	public function dataPropviderForIsMale() {
		return [
			[SexEnumInterface::FEMALE, false],
			[SexEnumInterface::MALE, true],
			['some-value', '', false],
		];
	}

	/**
	 * @dataProvider dataPropviderForIsFemale
	 */
	public function testIsFemale(string $value, string $expectedResult) {
		$sex = new SexEnum($value);
		$this->assertEquals($expectedResult, $sex->isFemale());
	}

	public function dataPropviderForIsFemale() {
		return [
			[SexEnumInterface::FEMALE, true],
			[SexEnumInterface::MALE, false],
			['some-value', '', false],
		];
	}

	/**
	 * @dataProvider dataPropviderForStaticConstructor
	 */
	public function testStaticConstructor($enum, $expectedValue) {
		$this->assertEquals($expectedValue, (string) $enum);
	}

	public function dataPropviderForStaticConstructor() {
		return [
			[SexEnum::male(), SexEnumInterface::MALE],
			[SexEnum::female(), SexEnumInterface::FEMALE]
		];
	}
}