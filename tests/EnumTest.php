<?php
namespace Keepper\Lib\Enum\Tests;

use Keepper\Lib\Enum\Enum;

class EnumTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataPropviderForIs
	 */
	public function testIs(string $value, string $tested, bool $result) {
		$enum = new Enum($value);
		$this->assertEquals($result, $enum->is($tested), 'Ожидали иной результат проверки при установленом значении '.$value.' И тестируемом ->is('.$tested.')');
		$this->assertEquals(!$result, $enum->isNot($tested), 'Ожидали иной результат проверки при установленом значении '.$value.' И тестируемом ->isNot('.$tested.')');
	}

	public function dataPropviderForIs() {
		return [
			['some-value', 'some-value', true],
			['some-value', 'some-value-1', false],
			['some-value', '', false],
		];
	}

	/**
	 * @dataProvider dataPropviderForIs
	 */
	public function testToString(string $value, string $tested, bool $result) {
		$enum = new Enum($value);
		$this->assertEquals($value, (string) $enum, 'Ожидали иное хранимое значение ('.$value.')');
	}

	/**
	 * @dataProvider dataPropviderForIsAre
	 */
	public function testIsAre(string $value, array $tested, bool $result) {
		$enum = new Enum($value);
		$this->assertEquals($result, call_user_func_array([$enum, 'is'], $tested), 'Ожидали иной результат проверки при установленом значении '.$value.' И тестируемом ->isAre('.print_r($tested, true).')');
		$this->assertEquals(!$result, call_user_func_array([$enum, 'isNot'], $tested), 'Ожидали иной результат проверки при установленом значении '.$value.' И тестируемом ->isNotAre('.print_r($tested, true).')');
	}

	public function dataPropviderForIsAre() {
		return [
			['some-value', [], false],
			['some-value', ['some-value-1', 'some-value-2'], false],
			['some-value', ['some-value-1', 'some-value-2', 'some-value'], true],
		];
	}
}